##############################################################################
# Helper define for splunk apps directories
##############################################################################
#
# This define creates the directory structure to hold splunk
# configuration files for an app.  An app is not really an
# application but a group of configuration files that are related.
#
# Example:
#
#    splunkforwarder::conf::splunk_app {'/opt/splunk/etc/apps/su_all_search_base':
#      ensure => present
#      files  => [ file1, file2, ... filen ]
#    }

define splunkforwarder::conf::splunk_app (
  $ensure = 'present',
  $files  = ''
) {

  $app_path = $name
  case $ensure {
    'absent': {
      file {
        $app_path:              ensure => absent;
        "${app_path}/local":    ensure => absent;
        "${app_path}/metadata": ensure => absent;
      }
    }
    'present': {
      file {
        $app_path:              ensure => directory;
        "${app_path}/local":    ensure => directory;
        "${app_path}/metadata": ensure => directory;
      }
      splunkforwarder::conf::splunk_app_file {"${app_path}/metadata/local.meta":
        ensure => present,
      }
      if $files != '' {
        splunkforwarder::conf::splunk_app_file {$files:
          ensure => present,
        }
      }
    }
    default: {
      fail ("Invalid ensure value: ${ensure}")
    }
  }
}
