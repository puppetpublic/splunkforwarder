##############################################################################
# Splunk Forwarder App Setup
##############################################################################
#
# This define creates the structures needed to define an input into
# splunk.  The input specification is specific to the configuration of
# an individual splunk forwarder.  The specific configuration is
# expected to be defined in the splunkforwarder_local module.  The
# files that must be defined in the splunkforwarder_local module for
# an individual app are described below.
#
# The files are all defined as templates even it is unlikely to
# require any substititions are required.  This makes the forwarder
# definition more flexible while reducing module complexitity.
#
# Files
# -----
#
#  templates/opt/splunkforwarder/etc/apps/<appName>/local/app.conf.erb
#
#    This configuration file controls the management of the forwarder.
#    For Linux management is essentially disabled since the systems
#    are with puppet.  The following example should be sufficient for
#    all Linux systems.
#
#     [install]
#     state = enabled
#
#     [package]
#     check_for_updates = false
#
#     [ui]
#     is_visible = false
#     is_manageable = false
#
#  templates/opt/splunkforwarder/etc/apps/<appName>/local/inputs.conf.erb
#
#    This file defines the inputs and will be unique to an individual
#    forwarder.  Below is an example fragment from a syslog server.
#
#     [monitor:///var/log/remote/core.log*]
#     index = unix_core
#     sourcetype = syslog
#     disabled = false
#
#     [monitor:///var/log/remote/network.log*]
#     index = network
#     sourcetype = syslog
#     disabled = false
#
#  templates/opt/splunkforwarder/etc/apps/<appName>/metadata/local.meta.erb
#
#    This file metadata file is the same for all apps.
#
#     []
#     access = read : [ * ], write : [ admin ]
#     export = system

define splunkforwarder::conf::forwarder_app (
  $forwarder_id = 'NONE',
) {

  $app_in = "/opt/splunkforwarder/etc/apps/${name}_inputs"
  splunkforwarder::conf::splunk_app { $app_in:
    ensure => present,
    files  => [
      "${app_in}/local/app.conf",
      "${app_in}/local/inputs.conf",
      ],
  }

  # The outputs file for forwarders has a password in it.  Even though
  # this is not a particularly secret password it is treated as any
  # other password in that it is stored in wallet and substituted into
  # the file configuration file.
  $app_out = "/opt/splunkforwarder/etc/apps/${name}_outputs"
  splunkforwarder::conf::splunk_app { $app_out:
    ensure => present,
    files  => [
      "${app_out}/local/limits.conf",
      ],
  }

  $app_outputs = "${app_out}/local/outputs.conf"
  file { "${app_outputs}.tmpl":
    content => template("splunkforwarder_local${app_outputs}.tmpl.erb"),
    notify  => Exec["generate-conf ${app_outputs}"],
    require => [Splunkforwarder::Conf::Splunk_app[$app_out]],
  }

  $app_out_1 = "generate-conf --template ${app_outputs}.tmpl"
  $app_out_2 = '--config /etc/ssl/private/splunk.forwarder.subs'
  $app_out_3 = "--newfile ${app_outputs}"
  exec { "generate-conf ${app_outputs}":
    command     => "${app_out_1} ${app_out_2} ${app_out_3}",
    refreshonly => true,
  }
}
