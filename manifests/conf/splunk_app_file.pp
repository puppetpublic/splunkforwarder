##############################################################################
# Helper define for splunk apps configuration templates
##############################################################################
#
# This define provides an additional level of indirection and makes
# it simpler to handle arrays of files.
#
# This define drops files into a splunk app directory.  All of the
# sources are templates even if there is no substitution being
# performing to keep app configuration files together.  
#
# Example:
#
#    $in_dir = '/opt/splunkforwarder/etc/apps'
#    splunkforwarder::conf::splunk_app_file {
#      "${in_dir}/su_all_search/distsearch.conf": ensure => present,
#    }

define splunkforwarder::conf::splunk_app_file (
  $ensure = 'present'
) {
  file {
    $name:
      mode    => '0644',
      content => template("splunkforwarder_local/${name}.erb");
  }
}
